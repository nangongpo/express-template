import { login, register, updatePassword, update, logout, getInfo } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import router, { resetRouter } from '@/router'

const state = {
  token: getToken(),
  username: sessionStorage.getItem('username'),
  avatar: '/avatar.gif',
  introduction: '',
  roles: [],
  allOptions: {
    company_info: [],
    express_state: [],
    send_record_status: [],
    username: '',
    password: '',
    name: '',
    phone: '',
    id_card: '',
    email: '',
    addr: '' // 收货地址, 寄件地址
  }
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_INTRODUCTION: (state, introduction) => {
    state.introduction = introduction
  },
  SET_NAME: (state, name) => {
    sessionStorage.setItem('username', name)
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  SET_OPTIONS: (state, options) => {
    state.allOptions = options
    console.log(state.allOptions)
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      login({ username: username.trim(), password: password }).then(data => {
        const { TOKEN } = data.response_data || {}
        commit('SET_TOKEN', TOKEN)
        commit('SET_NAME', username)
        setToken(TOKEN)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo().then(data => {
        const roles = ['admin']
        commit('SET_OPTIONS', data.response_data || {})
        commit('SET_ROLES', roles)
        resolve({ roles })
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state, dispatch }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        commit('SET_TOKEN', '')
        commit('SET_ROLES', [])
        removeToken()
        resetRouter()

        // reset visited views and cached views
        // to fixed https://github.com/PanJiaChen/vue-element-admin/issues/2485
        dispatch('tagsView/delAllViews', null, { root: true })

        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      commit('SET_ROLES', [])
      removeToken()
      resolve()
    })
  },

  register({ commit }, userInfo) {
    return new Promise((resolve, reject) => {
      register(userInfo).then(response => {
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  updatePassword({ commit }, userInfo) {
    return new Promise((resolve, reject) => {
      updatePassword(userInfo).then(response => {
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  update({ commit }, userInfo) {
    return new Promise((resolve, reject) => {
      update(userInfo).then(response => {
        // const { data } = response
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // dynamically modify permissions
  async changeRoles({ commit, dispatch }, role) {
    const token = role + '-token'

    commit('SET_TOKEN', token)
    setToken(token)

    const { roles } = await dispatch('getInfo')

    resetRouter()

    // generate accessible routes map based on roles
    const accessRoutes = await dispatch('permission/generateRoutes', roles, { root: true })
    // dynamically add accessible routes
    router.addRoutes(accessRoutes)

    // reset visited views and cached views
    dispatch('tagsView/delAllViews', null, { root: true })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
