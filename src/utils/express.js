export default {
  //  运单签收状态
  state: {
    '0': 'primary', // 在途
    '1': 'success', // 揽收
    '2': 'warning', // 疑难
    '3': 'success', // 签收
    '4': 'danger', // 退签
    '5': 'success', // 派件
    '6': 'danger', // 退回
    '7': 'success', // 转单
    '10': 'info', // 待清关
    '11': 'info', // 清关中
    '12': 'info', // 已清关
    '13': 'info', // 清关异常
    '14': 'danger' // 收件人拒签
  },
  // 寄件信息状态
  status: {
    '00': 'primary', // 准备下单
    '01': 'warning', // 下单中
    '02': 'success', // 下单成功
    '03': 'danger', // 下单失败
    '04': 'info' // 取消
  }
}
