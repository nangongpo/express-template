import request from '@/utils/request'

function formatData(state, data = {}) {
  return {
    method: 'post',
    url: '/gateway/',
    data: {
      method: 'express.express_info.manage',
      content: {
        state,
        ...data
      }
    }
  }
}

export function getExpressInfo(data) {
  return request(formatData('select', data))
}

export function getMapTrack(data) {
  return request(formatData('maptrack', data))
}
