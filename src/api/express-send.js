import request from '@/utils/request'

function formatData(state, data = {}) {
  return {
    method: 'post',
    url: '/gateway/',
    data: {
      method: 'express.send.manage',
      content: {
        state,
        ...data
      }
    }
  }
}

export function getShipperCompany(data) {
  return request(formatData('querymkt', data))
}

export function createOrder(data) {
  return request(formatData('place_order', data))
}

export function cancelOrder(data) {
  return request(formatData('cancel', data))
}

export function selectOrder(data) {
  return request(formatData('select', data))
}

