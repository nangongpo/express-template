import request from '@/utils/request'

function formatData(state, data = {}) {
  return {
    method: 'post',
    url: '/gateway/',
    data: {
      method: 'express.login.manage',
      content: {
        state,
        ...data
      }
    }
  }
}

export function register(data) {
  return request(formatData('register', data))
}

export function login(data) {
  return request(formatData('login', data))
}

export function logout() {
  return request(formatData('logout'))
}

export function updatePassword(data) {
  return request(formatData('update_password', data))
}

export function update(data) {
  return request(formatData('update', data))
}

export function getInfo(token) {
  return request({
    url: '/get_settings/',
    method: 'get',
    params: { token }
  })
}
