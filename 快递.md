[toc]

# 测试地址：http://49.235.127.165/express/

# 1.获取系统配置

- 接口说明
```
用于获取系统配置
```

- 请求方式
```
GET
```

- 请求地址
```
http://127.0.0.1:8000/express/get_settings/
```

- 返回参数
```
{
    "data_type": "1",
    "code": "1000",
    "msg": "",
    "response_data": {
        "company_info": [  # 快递公司信息
            {
                "label": "一智通",  # 快递公司名称
                "value": "1ziton"  # 快递公司编码
            }
        ],
        "express_state": [  # 快递状态
            {
                "label": "在途",  # 中文描述
                "value": "0"  # 编码
            }
        ],
        "send_record_status": [  # 寄件信息状态
            {
                "label": "准备下单",
                "value": "00"
            },
            {
                "label": "下单中",
                "value": "01"
            },
            {
                "label": "下单成功",
                "value": "02"
            },
            {
                "label": "下单失败",
                "value": "03"
            },
            {
                "label": "取消",
                "value": "04"
            }
        ]
    },
    "timestamp": "2021-05-16 16:14:20"
}
```

# 2.用户注册

- 接口说明
```
用户注册
```

- 请求方式
```
POST
```

- 请求地址
```
http://127.0.0.1:8000/express/gateway/
```
- 请求参数
```
{
    "method": "express.login.manage",
    "content": {
        "state": "register",
        "username": "000000",  # 用户名，必填
        "password": "000000",  # 密码，必填
        "name": "靳佳康",  # 姓名，必填
        "phone": "15111111111",  # 手机号，必填
        "id_card": "411111111111111111",  # 身份证号，必填
        "email": "296508600@qq.com",  # 邮箱，必填
        "addr": "河南省郑州市",  # 地址，必填
    }
}
```


- 返回参数
```
{
    "data_type": "1",
    "code": "1000",
    "msg": "",
    "response_data": "",
    "timestamp": "2021-05-16 15:18:36"
}
```

# 3.用户登录

- 接口说明
```
用户登录
```

- 请求方式
```
POST
```

- 请求地址
```
http://127.0.0.1:8000/express/gateway/
```
- 请求参数
```
{
    "method": "express.login.manage",
    "content": {
        "state": "login",
        "username": "000000",  # 必填
        "password": "000000"  # 必填
    }
}
```


- 返回参数
```
{
    "data_type": "1",
    "code": "1000",
    "msg": "",
    "response_data": {
        "TOKEN": "c45576fa-3a5c-462a-b2ab-c8b0c9be86f1"  # 用于验证用户是否登录
    },
    "timestamp": "2021-05-16 15:30:43"
}
```

# 4.用户注销

- 接口说明
```
用户注销
请求头中需要传TOKEN
```

- 请求方式
```
POST
```

- 请求地址
```
http://127.0.0.1:8000/express/gateway/
```
- 请求参数
```
{
    "method": "express.login.manage",
    "content": {
        "state": "logout"
    }
}
```


- 返回参数
```
{
    "data_type": "1",
    "code": "1000",
    "msg": "",
    "response_data": "",
    "timestamp": "2021-05-16 15:21:15"
}
```

# 5.用户修改信息

- 接口说明
```
用户修改信息，不包括密码
```

- 请求方式
```
POST
```

- 请求地址
```
http://127.0.0.1:8000/express/gateway/
```
- 请求参数
```
{
    "method": "express.login.manage",
    "content": {
        "state": "update",  
        "username": "000000",  # 必填
        "password": "000000",  # 必填
        "name": "靳佳康",  # 必填
        "phone": "15111111112",  # 必填
        "id_card": "411111111111111112",  # 必填
        "email": "296508600@qq.com",  # 必填
        "addr": "河南省郑州市",  # 地址，必填
    }
}
```


- 返回参数
```
{
    "data_type": "1",
    "code": "1000",
    "msg": "",
    "response_data": "",
    "timestamp": "2021-05-16 15:21:15"
}
```
# 5.用户修改密码
- 接口说明
```
用户修改密码
```

- 请求方式
```
POST
```

- 请求地址
```
http://127.0.0.1:8000/express/gateway/
```
- 请求参数
```
{
    "method": "express.login.manage",
    "content": {
        "state": "update_password",  
        "username": "000000",  # 必填
        "password": "000000",  # 必填
        "new_password": "000000",  # 必填
    }
}
```


- 返回参数
```
{
    "data_type": "1",
    "code": "1000",
    "msg": "",
    "response_data": "",
    "timestamp": "2021-05-16 15:21:15"
}
```



# 6.用户查询快递信息

- 接口说明
```
用户查询快递信息
请求头中需要传TOKEN
```

- 请求方式
```
POST
```

- 请求地址
```
http://127.0.0.1:8000/express/gateway/
```
- 请求参数
```
{
    "method": "express.express_info.manage",
    "content": {
        "state": "select",
        "com": "jd",  # 快递公司编码，必填
        "num": "JD0042307996792"  # 运单号，必填
    }
}
```


- 返回参数
```
{
    "data_type": "1",
    "code": "1000",
    "msg": "",
    "response_data": {
        "state": "3",  # 快递状态
        "com": "jd",  # 快递公司编号
        "nu": "JD0042307996792",  # 运单号
        "data": [  # 快递详细信息
            {
                "time": "2021-05-09 09:39:51",
                "context": "您的快件已由客户指定地点代收，感谢您使用京东物流，期待再次为您服务",
                "ftime": "2021-05-09 09:39:51",
                "areaCode": null,
                "areaName": null,
                "status": "签收"
            },
            {
                "time": "2021-05-09 08:01:37",
                "context": "您的快件正在派送中，请您准备签收（快递员：刘武佳，联系电话：15639144275）",
                "ftime": "2021-05-09 08:01:37",
                "areaCode": null,
                "areaName": null,
                "status": "派件"
            },
            {
                "time": "2021-05-09 07:30:16",
                "context": "您的快件已到达【郑州红城营业部】",
                "ftime": "2021-05-09 07:30:16",
                "areaCode": null,
                "areaName": null,
                "status": "在途"
            },
            {
                "time": "2021-05-09 07:30:15",
                "context": "您的快件在【郑州红城营业部】收货完成",
                "ftime": "2021-05-09 07:30:15",
                "areaCode": null,
                "areaName": null,
                "status": "在途"
            },
            {
                "time": "2021-05-09 02:05:50",
                "context": "您的快件已发车",
                "ftime": "2021-05-09 02:05:50",
                "areaCode": null,
                "areaName": null,
                "status": "在途"
            },
            {
                "time": "2021-05-09 00:30:07",
                "context": "您的快件由【郑州前程分拣中心】准备发往【郑州红城营业部】",
                "ftime": "2021-05-09 00:30:07",
                "areaCode": "CN410100000000",
                "areaName": "河南,郑州市",
                "status": "在途"
            },
            {
                "time": "2021-05-09 00:30:02",
                "context": "您的快件在【郑州前程分拣中心】分拣完成",
                "ftime": "2021-05-09 00:30:02",
                "areaCode": "CN410100000000",
                "areaName": "河南,郑州市",
                "status": "在途"
            },
            {
                "time": "2021-05-09 00:27:35",
                "context": "您的快件已到达【郑州前程分拣中心】",
                "ftime": "2021-05-09 00:27:35",
                "areaCode": null,
                "areaName": null,
                "status": "在途"
            },
            {
                "time": "2021-05-09 00:20:30",
                "context": "您的快件由【郑州接货仓】准备发往【郑州前程分拣中心】",
                "ftime": "2021-05-09 00:20:30",
                "areaCode": "CN410100000000",
                "areaName": "河南,郑州市",
                "status": "在途"
            },
            {
                "time": "2021-05-09 00:20:25",
                "context": "您的快件在【郑州接货仓】分拣完成",
                "ftime": "2021-05-09 00:20:25",
                "areaCode": "CN410100000000",
                "areaName": "河南,郑州市",
                "status": "揽收"
            }
        ]
    },
    "timestamp": "2021-05-16 16:08:14"
}
```


# 7.用户查看快递地图轨迹

- 接口说明
```
用户查看快递地图轨迹
请求头中需要传TOKEN
```

- 请求方式
```
POST
```

- 请求地址
```
http://127.0.0.1:8000/express/gateway/
```
- 请求参数
```
{
    "method": "express.express_info.manage",
    "content": {
        "state": "maptrack",
        "com": "jd",  # 必填
        "num": "JD0042307996792",  # 必填
        "from": "河南郑州市",  # 出发地， 必填
        "to": "河南郑州市中原区冬青街与雪松路交叉口西南90米秦庄嘉园"  # 目的地，必填
    }
}
```


- 返回参数
```
{
    "data_type": "1",
    "code": "1000",
    "msg": "",
    "response_data": {
        "nu": "JD0042307996792",  # 运单号
        "com": "jd",  # 快递公司编号
        "state": "3",  # 快递状态
        "trailUrl": "https://api.kuaidi100.com/tools/map/906838d7e6916f54b365bea207634781_113.612850,34.748257_12",  # 地图轨迹URL
        "arrivalTime": "2021-05-09 11",  #预计到达时间
        "totalTime": "0天16小时",  # 平均耗时
        "remainTime": null,  # 到达还需要多少时间
        "data": [  # 快递详细信息
            {
                "time": "2021-05-09 09:39:51",  # 时间，原始格式
                "context": "您的快件已由客户指定地点代收，感谢您使用京东物流，期待再次为您服务",  # 内容
                "ftime": "2021-05-09 09:39:51",  # 格式化后时间
                "areaCode": null,  # 本数据元对应的行政区域的编码，只有实时查询接口中提交resultv2标记后才会出现
                "areaName": null,  # 本数据元对应的行政区域的名称，只有实时查询接口中提交resultv2标记后才会出现
                "status": "签收"  # 本数据元对应的签收状态，只有实时查询接口中提交resultv2标记后才会出现
            },
            {
                "time": "2021-05-09 08:01:37",
                "context": "您的快件正在派送中，请您准备签收（快递员：刘武佳，联系电话：15639144275）",
                "ftime": "2021-05-09 08:01:37",
                "areaCode": null,
                "areaName": null,
                "status": "派件"
            },
            {
                "time": "2021-05-09 07:30:16",
                "context": "您的快件已到达【郑州红城营业部】",
                "ftime": "2021-05-09 07:30:16",
                "areaCode": null,
                "areaName": null,
                "status": "在途"
            },
            {
                "time": "2021-05-09 07:30:15",
                "context": "您的快件在【郑州红城营业部】收货完成",
                "ftime": "2021-05-09 07:30:15",
                "areaCode": null,
                "areaName": null,
                "status": "在途"
            },
            {
                "time": "2021-05-09 02:05:50",
                "context": "您的快件已发车",
                "ftime": "2021-05-09 02:05:50",
                "areaCode": null,
                "areaName": null,
                "status": "在途"
            },
            {
                "time": "2021-05-09 00:30:07",
                "context": "您的快件由【郑州前程分拣中心】准备发往【郑州红城营业部】",
                "ftime": "2021-05-09 00:30:07",
                "areaCode": "CN410100000000",
                "areaName": "河南,郑州市",
                "status": "在途"
            },
            {
                "time": "2021-05-09 00:30:02",
                "context": "您的快件在【郑州前程分拣中心】分拣完成",
                "ftime": "2021-05-09 00:30:02",
                "areaCode": "CN410100000000",
                "areaName": "河南,郑州市",
                "status": "在途"
            },
            {
                "time": "2021-05-09 00:27:35",
                "context": "您的快件已到达【郑州前程分拣中心】",
                "ftime": "2021-05-09 00:27:35",
                "areaCode": null,
                "areaName": null,
                "status": "在途"
            },
            {
                "time": "2021-05-09 00:20:30",
                "context": "您的快件由【郑州接货仓】准备发往【郑州前程分拣中心】",
                "ftime": "2021-05-09 00:20:30",
                "areaCode": "CN410100000000",
                "areaName": "河南,郑州市",
                "status": "在途"
            },
            {
                "time": "2021-05-09 00:20:25",
                "context": "您的快件在【郑州接货仓】分拣完成",
                "ftime": "2021-05-09 00:20:25",
                "areaCode": "CN410100000000",
                "areaName": "河南,郑州市",
                "status": "揽收"
            }
        ],
        "routeInfo": {  # 路由信息
            "from": {  # 出发地行政区信息
                "number": "CN4101", # 政区信息编码
                "name": "河南,郑州市"  # 政区信息名
            },
            "cur": {  # 当前地行政区信息
                "number": "CN4101",
                "name": "河南,郑州市"
            },
            "to": {  # 目的地行政区信息
                "number": "CN4101",
                "name": "河南,郑州市"
            }
        }
    },
    "timestamp": "2021-05-16 16:21:38"
}

```

# 8.查询可以寄件的快递公司

- 接口说明
```
查询可以寄件的快递公司
请求头中需要传TOKEN
```

- 请求方式
```
POST
```

- 请求地址
```
http://127.0.0.1:8000/express/gateway/
```
- 请求参数
```
{
    "method": "express.send.manage",
    "content": {
        "state": "querymkt",
        "sendAddr": "河南省郑州市金水区郑州市动物园"  # 出发地，非必填，当用户不填写时，则使用用户创建账号时填写的地址
    }
}
```


- 返回参数
```
{
    "data_type": "1",
    "code": "1000",
    "msg": "",
    "response_data": {
        "province": "河南",  # 寄件人地址，省
        "city": "郑州市",  # 寄件人地址，市
        "district": "金水区",  # 寄件人地址，区
        "latitude": "34.7889696654452",  # 经度
        "mktInfo": [  # 覆盖运力快递公司列表
            {
                "serviceType": [  # 业务服务类型
                    "标准快递"
                ],
                "com": "yuantong",  # 快递公司编码
            }
        ],
        "addr": "郑州市动物园",  # 地址
        "longitude": "113.68464302502011"  # 纬度
    },
    "timestamp": "2021-05-16 17:15:52"
}

```

# 9.寄件下单接口

- 接口说明
```
寄件下单接口
请求头中需要传TOKEN
```

- 请求方式
```
POST
```

- 请求地址
```
http://127.0.0.1:8000/express/gateway/
```
- 请求参数
```
{
    "method": "express.send.manage",
    "content": {
        "state": "place_order",
        "serviceType": "标准快递",  # 业务服务类型,必填
        "com": "yuantong",  # 快递公司编码，必填
        "recManName": "靳佳康",  # 收件人姓名，必填
        "recManMobile": "15036505580",  # 收件人手机号，必填
        "recManPrintAddr": "河南省辉县市薄壁镇铁匠庄村",  # 收件人地址，必填
        "sendManName": "靳佳康",  # 寄件人姓名，非必填，如果用户不填，则默认使用该用户注册时的信息
        "sendManMobile": "15036505580",  # 寄件人手机号，非必填，如果用户不填，则默认使用该用户注册时的信息
        "sendManPrintAddr": "河南省郑州市金水区郑州市动物园",  # 寄件人地址，非必填，如果用户不填，则默认使用该用户注册时的信息
        "cargo": "杂物",  # 物品名称
        "weight": "10",  # 物品重量，单位KG
        "remark": "测试"  # 备注
    }
}
```


- 返回参数
```
{
    "data_type": "1",
    "code": "1000",
    "msg": "",
    "response_data": "",
    "timestamp": "2021-05-16 19:29:40"
}

```

# 10.寄件取消接口

- 接口说明
```
寄件取消接口
请求头中需要传TOKEN
```

- 请求方式
```
POST
```

- 请求地址
```
http://127.0.0.1:8000/express/gateway/
```
- 请求参数
```
{
    "method": "express.send.manage",
    "content": {
        "state": "cancel",
        "id": 2,  # 寄件记录ID， 必填
        "cancelMsg": "不想寄了"  # 取消寄件原因，必填
    }
}
```


- 返回参数
```
{
    "data_type": "1",
    "code": "1000",
    "msg": "",
    "response_data": "",
    "timestamp": "2021-05-16 19:29:40"
}

```

# 11.寄件信息查询接口

- 接口说明
```
寄件信息查询接口
请求头中需要传TOKEN
```

- 请求方式
```
POST
```

- 请求地址
```
http://127.0.0.1:8000/express/gateway/
```
- 请求参数
```
# 这里我把所有的字段都弄成可查询的了，你可以选择性的用，不是必须要把所有字段都弄成查询条件的
{
    "method": "express.send.manage",
    "content": {
        "state": "select",
        "id": 2,  # 寄件信息ID
        "com": "yuantong",  # 寄件公司编号
        "recManName": "靳",  # 收件人姓名，模糊查询
        "recManMobile": "150",  # 收件人手机号，模糊查询
        "recManPrintAddr": "河南",  # 收件人地址，，模糊查询
        "sendManName": "靳",  # 寄件人姓名，模糊查询
        "sendManMobile": "150",  # 寄件人手机号，模糊查询
        "sendManPrintAddr": "河南",  # 寄件人地址，模糊查询
        "cargo": "物",  # 物品名称，模糊查询
        "weight": "10",  # 物品重量，单位KG
        "serviceType": "快递",  # 服务类型，模糊查询
        "remark": "测试",  # 备注，模糊查询
        "status": "01",  # 寄件信息状态
        "cancelMsg": "",  # 取消原因，模糊查询
        "create_time": ["2021-01-01 00:00:00", "2021-10-10 00:00:00"],
        "update_time": ["2021-01-01 00:00:00", "2021-10-10 00:00:00"]
    }
}
```


- 返回参数
```
{
    "data_type": "1",
    "code": "1000",
    "msg": "",
    "response_data": [
        {
            "id": 2,
            "user": "000000",
            "com": "yuantong",
            "recManName": "靳佳康",
            "recManMobile": "15036505580",
            "recManPrintAddr": "河南省辉县市薄壁镇铁匠庄村",
            "sendManName": "靳佳康",
            "sendManMobile": "15036505580",
            "sendManPrintAddr": "河南省郑州市金水区郑州市动物园",
            "cargo": "杂物",
            "weight": "10",
            "serviceType": "标准快递",
            "remark": "测试",
            "status": "靳佳康",
            "cancelMsg": null,
            "create_time": "2021-05-16 19:29:39",
            "update_time": "2021-05-16 19:29:40"
        }
    ],
    "timestamp": "2021-05-16 20:13:35"
}

```

# 12.寄件回调接口
- 接口说明
```
寄件回调接口
这个是给快递100用的接口，前端可以忽略
```

- 请求方式
```
POST
```

- 请求地址
```
http://127.0.0.1:8000/express/gateway/
```
- 请求参数
```
# 这里我把所有的字段都弄成可查询的了，你可以选择性的用，不是必须要把所有字段都弄成查询条件的
{
    "method": "express.send.manage",
    "content": {
        "state": "select",
        "id": 2,  # 寄件信息ID
        "com": "yuantong",  # 寄件公司编号
        "recManName": "靳",  # 收件人姓名，模糊查询
        "recManMobile": "150",  # 收件人手机号，模糊查询
        "recManPrintAddr": "河南",  # 收件人地址，，模糊查询
        "sendManName": "靳",  # 寄件人姓名，模糊查询
        "sendManMobile": "150",  # 寄件人手机号，模糊查询
        "sendManPrintAddr": "河南",  # 寄件人地址，模糊查询
        "cargo": "物",  # 物品名称，模糊查询
        "weight": "10",  # 物品重量，单位KG
        "serviceType": "快递",  # 服务类型，模糊查询
        "remark": "测试",  # 备注，模糊查询
        "status": "01",  # 寄件信息状态
        "cancelMsg": "",  # 取消原因，模糊查询
        "create_time": ["2021-01-01 00:00:00", "2021-10-10 00:00:00"],
        "update_time": ["2021-01-01 00:00:00", "2021-10-10 00:00:00"]
    }
}
```


- 返回参数
```
{
    "data_type": "1",
    "code": "1000",
    "msg": "",
    "response_data": [
        {
            "id": 2,
            "user": "000000",
            "com": "yuantong",
            "recManName": "靳佳康",
            "recManMobile": "15036505580",
            "recManPrintAddr": "河南省辉县市薄壁镇铁匠庄村",
            "sendManName": "靳佳康",
            "sendManMobile": "15036505580",
            "sendManPrintAddr": "河南省郑州市金水区郑州市动物园",
            "cargo": "杂物",
            "weight": "01",  # 订单状态
            "serviceType": "标准快递",
            "remark": "测试",
            "status": "靳佳康",
            "cancelMsg": null,
            "create_time": "2021-05-16 19:29:39",
            "update_time": "2021-05-16 19:29:40"
        }
    ],
    "timestamp": "2021-05-16 20:13:35"
}

```